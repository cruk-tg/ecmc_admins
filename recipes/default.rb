#
# Cookbook:: ecmc_admins
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.
users_manage 'cruk-adm' do
  group_id 3001
  action [:create]
  data_bag 'users'
end
