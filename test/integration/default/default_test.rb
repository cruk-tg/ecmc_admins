# # encoding: utf-8

# Inspec test for recipe ecmc_admins::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe user('test_user') do
  it { should exist }
end

describe group('cruk-adm') do
  it { should exist }
end
